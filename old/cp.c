#include <semaphore.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#define MIN_NUMBER 1
#define MAX_NUMBER 100
#define BUFFCELLS 100



int buf[BUFFCELLS];
sem_t buffSem;
sem_t itemSem;
sem_t prodCounterSem;
int itemsInBuff=0;
int nProducers;
int nActiveProducers;
int nConsumers;
int idCons = 0;

int generate(){
	int result;
	srand(time(NULL));
	result = rand() % (MAX_NUMBER + 1 - MIN_NUMBER ) + MIN_NUMBER;
	return result;
}

void* produce(void *m){
	int counter = 0;
	while(counter<1000000){
		usleep(rand()%(10)+1);
		int number = generate();
		// fprintf(stderr, "%d\n", number);
		sem_wait(&itemSem);
		if(itemsInBuff<BUFFCELLS){
			sem_wait(&buffSem);
			buf[itemsInBuff]=number;
			// fprintf(stderr, "%d\n", buf[itemsInBuff]);
			sem_post(&buffSem);
			itemsInBuff++;
		}
		sem_post(&itemSem);
		counter++;
	}
	// sem_wait(&prodCounterSem);
	nActiveProducers--;
	// sem_post(&prodCounterSem);
	pthread_exit(NULL);
}

void* consume(void *m){
	int sum = 0;
	int id = idCons++;
	// sem_wait(&prodCounterSem);
	while(nActiveProducers>0 || itemsInBuff>0){
		// fprintf(stderr, "mpika while\n");
		sem_wait(&itemSem);
		if(itemsInBuff>0){
			sem_wait(&buffSem);
			// fprintf(stderr, " consumer sees: %d, next is: %d \n", buf[itemsInBuff-1], buf[itemsInBuff] );
			sum += buf[itemsInBuff-1];
			buf[itemsInBuff-1] = 0;
			sem_post(&buffSem);
			itemsInBuff--;
		}
		sem_post(&itemSem);
	}

	fprintf(stderr, "consumer %d sum = %d \n", id, sum);
	pthread_exit(NULL);
}


int main(int argc, char * argv[]){

	fprintf(stderr, "%d producers,  %d consumers\n", atoi(argv[1]), atoi(argv[2]));
	nProducers = atoi(argv[1]);
	nConsumers = atoi(argv[2]);
	nActiveProducers = nProducers;
	pthread_t tProd[nProducers];
	pthread_t tCons[nConsumers];
	
	sem_init(&buffSem, 0, 1);

	sem_init(&itemSem, 0, 1);

	sem_init(&prodCounterSem, 0, 1);

	int i=0;
	while(i<nProducers){
		pthread_create(&tProd[i], NULL, produce, NULL);
		i++;
	}

	i=0;
	while(i<nConsumers){
		pthread_create(&tCons[i], NULL, consume, NULL);
		i++;
	}


	i=0;
	while(i<nProducers){
		pthread_join(tProd[i], NULL);
		i++;
	}

	i=0;
	while(i<nConsumers){
		pthread_join(tCons[i], NULL);
		i++;
	}

	return 0;
}

