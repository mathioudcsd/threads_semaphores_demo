#include <stdlib.h>
#include <semaphore.h>
#define MIN_NUMBER 1
#define MAX_NUMBER 100
#define BUFFCELLS 10000


extern int nProducers;
extern int nConsumers;
extern int buf[BUFFCELLS];
//extern int lastFilledIndex;
extern int itemsInBuff;
extern sem_t buffSem;				// buffer semaphore
extern sem_t itemSem;				// itemsInBuff 
extern sem_t prodCounterSem;


int generate(void);

void* produce(void *m);

void* consume(void *m);

