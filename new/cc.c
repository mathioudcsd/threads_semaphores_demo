#include <semaphore.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <stdint.h>
#define MAX_CHAIRS 500
#define MAX_CLIENTS 10000
#define MIN_NUMBER 0
#define MAX_NUMBER 400
#define true 1
#define false 0

/* for the items below, synchronisation and mutual exclusion will be needed */
// int grocers_asleep[]={1,1}; // 1 for being asleep, 0 for awake 
sem_t timeSEM;
sem_t ccounterSEM;
sem_t nextClientsSEM;
sem_t satisfied_customersSEM;
sem_t clientsLeavingSEM;


int total_wait_time=0; // in MilliSeconds
int satisfied_customers=0;
int leaving_clients=0;
int next_clients[2];
int no_more_clients_to_come;


int chairs;
int clients;
int ccounter=0; //POSOI KATHONTAI panta prepei na einai < chairs

int treturn;

int generate(){
	int result;
	srand(time(NULL));
	result = rand() % (MAX_NUMBER + 1 - MIN_NUMBER ) + MIN_NUMBER;
	return result;
}

void check_args(int argc, char* argv[])
{
	int chairs = atoi(argv[2]);
	int clients = atoi(argv[4]);

	fprintf(stdout,"chairs: %d, clients: %d\n\n", chairs, clients);
	if(argc!=5 || (strcmp(argv[1],"chairs")!=0) || (chairs<1 || chairs>MAX_CHAIRS) || (strcmp(argv[3],"clients")!=0) || (clients<1 || clients>MAX_CLIENTS))
		{
			fprintf(stderr, "Usage: ./exe chairs 500 clients 10000 || Max values shown in example\n\n");
			exit(1);
		}
}

void* client(int i){
	struct timespec start_timestamp;
	struct timespec end_timestamp;
	int64_t start_millitime;
	int64_t end_millitime;

	int finished=false;
	fprintf(stdout, "I am client Number %d \n", i);
	
	if(ccounter == chairs){
		fprintf(stdout, "FEVGEI PELATHS\n");
		//sem_wait(&clientsLeavingSEM);
		leaving_clients++;
		//sem_post(&clientsLeavingSEM);
		pthread_exit(0);
	}
	else{
		clock_gettime(CLOCK_REALTIME,&start_timestamp);
		start_millitime =  start_timestamp.tv_sec * INT64_C(1000) + start_timestamp.tv_nsec / 1000000;
		sem_wait(&ccounterSEM);
		ccounter++;
		sem_post(&ccounterSEM);
		while(finished!=true){
			sem_wait(&nextClientsSEM);
			if(next_clients[0]==-1){
				next_clients[0]=i;
				finished=true;
				sem_wait(&ccounterSEM);
				ccounter--;
				sem_post(&ccounterSEM);
			}
			if(next_clients[1]==-1){
				next_clients[1]=i;
				finished=true;
				sem_wait(&ccounterSEM);
				ccounter--;
				sem_post(&ccounterSEM);
			}
			sem_post(&nextClientsSEM);
		}
		clock_gettime(CLOCK_REALTIME,&end_timestamp);
		end_millitime = end_timestamp.tv_sec * INT64_C(1000) + end_timestamp.tv_nsec / 1000000;
		sem_wait(&timeSEM);
		sem_wait(&satisfied_customersSEM);
		total_wait_time+=(end_millitime-start_millitime);
		satisfied_customers++;
		sem_post(&satisfied_customersSEM);
		sem_post(&timeSEM); 	
	}
	pthread_exit(0);
}

void* grocer(int i){
	int randomSleepTime;
	while(no_more_clients_to_come == false){
		//fprintf(stdout, "GAMW_1 \n");
		while(ccounter>0){
			fprintf(stdout, "GAMW_2 \n");
			sem_wait(&nextClientsSEM);
			fprintf("o grocer %d vlepei [%d,%d]\n", next_clients[0], next_clients[1]);
			if(next_clients[0]!=-1){
				randomSleepTime=generate();
				usleep(randomSleepTime);
				next_clients[0]=-1;
				sem_post(&nextClientsSEM);
				continue;
			}
			if(next_clients[1]!=1){
				fprintf(stdout, "GAMW_3 \n");
				randomSleepTime=generate();
				usleep(randomSleepTime);
				next_clients[1]=-1;
				sem_post(&nextClientsSEM);
				continue;
			}
		}
	}
	while(ccounter>0){
		sem_wait(&nextClientsSEM);
		if(next_clients[0]!=-1){
			randomSleepTime=generate();
			usleep(randomSleepTime);
			next_clients[0]=-1;
			sem_post(&nextClientsSEM);
			continue;
		}
		if(next_clients[1]!=1){
			randomSleepTime=generate();
			usleep(randomSleepTime);
			next_clients[1]=-1;
			sem_post(&nextClientsSEM);
			continue;
		}
	}
}

int main(int argc, char* argv[]){ 
	check_args(argc, argv);
	chairs = atoi(argv[2]);
	clients = atoi(argv[4]);	
	if(argv[4]>0)
		no_more_clients_to_come=false;
	pthread_t tGrocer[2];
	pthread_t tClient[clients];

	int i=0;
	while(i<2){
		pthread_create(&tGrocer[i], NULL, grocer, i);
		i++;
	}
	fprintf(stdout, "All Grocers Created\n\n");

	i=0;
	while(i<2){
		pthread_join(tGrocer[i], NULL);
		fprintf(stdout, "tGrocer %d joined \n", (int) i);
		i++;
	}
	fprintf(stdout, "All Grocers Joined\n\n");

	i=0;
	while(i<clients){
		usleep(1000);
		pthread_create(&tClient[i], NULL, client, (int) i);
		i++;
		ccounter++;
	}
	fprintf(stdout, "All Clients Created (and JOINED?)\n\n");
	

	// i=0;
	// while(i<clients){
	// 	// 1 millisecond  = 1000 mikroseconds
	// 	//usleep(900); // 100.000
	// 	fprintf(stdout, "client %d joined\n",i );
	// 	pthread_join(tClient[i], NULL);
	// 	fprintf(stdout, "tclient %d joined \n", i);
	// 	i++;
	// 	ccounter++;
	// }
	// fprintf(stdout, "All Clients Joined\n\n");

	fprintf(stdout, "MORE CLIENTS? >>> %d\n", no_more_clients_to_come );
	no_more_clients_to_come=true;
	fprintf(stdout, "MORE CLIENTS? >>> %d\n", no_more_clients_to_come );

	//fprintf("Mesos xronos anamonis: %d MilliSeconds",total_wait_time );
	fprintf(stdout, "EFYGAN: %d\n", leaving_clients);
	return 0;
}






