#include "f.h"

int generate(){
	int result;
	srand(time(NULL));
	result = rand() % (MAX_NUMBER + 1 - MIN_NUMBER ) + MIN_NUMBER;
	return result;
}

void* produce(void *m){
	int counter = 0;
	while(counter<100){
		int number = generate();
		sem_wait(&itemSem);
		if(itemsInBuff<BUFFCELLS){
			sem_wait(&buffSem);
			buf[itemsInBuff-1]=number;
			sem_post(&buffSem);
			itemsInBuff++;
		}
		sem_post(&itemSem);
		counter++;
	}
	sem_wait(&prodCounterSem);
	nProducers--;
	sem_post(&prodCounterSem);
}

void* consume(void *m){
	int sum = 0;
	sem_wait(&prodCounterSem);
	while(nProducers>0){
		sem_wait(&itemSem);
		if(itemsInBuff>0){
			sem_wait(&buffSem);
			sum += buf[itemsInBuff-1];
			buf[itemsInBuff-1] = 0;
			sem_post(&buffSem);
			itemsInBuff--;
		}
		sem_post(&itemSem);
	}
}